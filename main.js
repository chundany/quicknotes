const electron = require('electron')
const {app, BrowserWindow} = electron
var fs = require('fs');
var os = require('os');
var path = require('path');

var settingspath = path.join(os.homedir(),'Documents','QuickNotes');
//create if not exist default files
try {
  fs.accessSync(path.join(settingspath));
} catch (e) {
  fs.mkdirSync(path.join(settingspath));
}
try {
  fs.accessSync(path.join(settingspath, 'notes.json'));
} catch (e) {
  fs.writeFileSync(path.join(settingspath, 'notes.json'), 
          '{"notes":[{"name": "note_1","content": "notes...","color":"lightblue","pos":{"x":50,"y":50,"width":431,"height":254}}], "nxt":[{"next": 3}]}');
}
var loadNotes = JSON.parse(fs.readFileSync(path.join(settingspath, 'notes.json'), 'utf8'));
//console.log(loadNotes.notes[0].pos.x)
let win;

function createWindow () {
  for (i=0; i<loadNotes.notes.length; i++){
  win = new BrowserWindow({
    'x': loadNotes.notes[i].pos.x,
    'y': loadNotes.notes[i].pos.y,
    'width': loadNotes.notes[i].pos.width,
    'height': loadNotes.notes[i].pos.height,
    'frame': false});

  win.setMenu(null);
  win.loadURL(`file://${__dirname}/index.html?${loadNotes.notes[i].name}`);
  //win.webContents.openDevTools()
  }
  win.on('close', () => {
  })

  win.on('closed', () => {
    win = null
  });
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow()
  }
})
