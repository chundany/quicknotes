const remote = require('electron').remote;
var fs = require('fs');
var os = require('os');
var path = require('path');
var settingspath = path.join(os.homedir(), 'Documents', 'QuickNotes');

class QuickNotes {
    constructor(note_num) {
        this.note_id = note_num;
        this.showControls = false;
        //console.log(this.note_id)
        this.bColor = "lightblue";
        

        let loadNotes = JSON.parse(fs.readFileSync(path.join(settingspath, 'notes.json'), 'utf8'));
        for (let nt in loadNotes.notes){
            if (loadNotes.notes[nt]["name"] == this.note_id){
                document.getElementById('txtarea').innerText = loadNotes.notes[nt]["content"];
                this.bColor = loadNotes.notes[nt]["color"];
                document.body.style.backgroundColor = this.bColor;
                break;
            }
        }
    }

    changeClr(color){
        this.bColor = color;        
        document.body.style.backgroundColor = this.bColor;
    }
    saveNotes(note){
        let loadNotes = JSON.parse(fs.readFileSync(path.join(settingspath, 'notes.json'), 'utf8'));
        for (let nt in loadNotes.notes){
            if (loadNotes.notes[nt]["name"] == this.note_id){
                loadNotes.notes[nt]["content"] = note;
                fs.writeFileSync(path.join(settingspath, 'notes.json'), JSON.stringify(loadNotes));
                break;
            }
        }
    }
    closeWin(){
        let window = remote.getCurrentWindow();
        let lstbounds = window.getBounds();
        let loadNotes = JSON.parse(fs.readFileSync(path.join(settingspath, 'notes.json'), 'utf8'));
        for (let nt in loadNotes.notes){
            if (loadNotes.notes[nt]["name"] === this.note_id){
                loadNotes.notes[nt].pos.x = lstbounds.x;
                loadNotes.notes[nt].pos.y = lstbounds.y;
                loadNotes.notes[nt].pos.width = lstbounds.width;
                loadNotes.notes[nt].pos.height = lstbounds.height;
                loadNotes.notes[nt]["color"] = this.bColor;
                fs.writeFileSync(path.join(settingspath, 'notes.json'), JSON.stringify(loadNotes));
                break;
            }
        }
        window.close();
    }
    delNote(){
        let loadNotes = JSON.parse(fs.readFileSync(path.join(settingspath, 'notes.json'), 'utf8'));
        for (let nt in loadNotes.notes){
            if (loadNotes.notes[nt]["name"] == this.note_id){
                loadNotes.notes.splice(nt ,1);
                fs.writeFileSync(path.join(settingspath, 'notes.json'), JSON.stringify(loadNotes));
                let window = remote.getCurrentWindow();
                window.close();
                break;
            }
        }
    }
    newNote(){
        let loadNotes = JSON.parse(fs.readFileSync(path.join(settingspath, 'notes.json'), 'utf8'));
        loadNotes['notes'].push({"name": "note_"+loadNotes.nxt[0].next,"content": "","color":"lightblue","pos":{"x":150,"y":150,"width":431,"height":254}});
        
        const BrowserWindow = remote.BrowserWindow;
        let win = new BrowserWindow({ width: 400, height: 200,'frame': false });
        win.setMenu(null);
        win.loadURL(window.location.href.split('?')[0]+"?note_"+loadNotes.nxt[0].next);
        loadNotes.nxt[0].next= loadNotes.nxt[0].next+1;

        fs.writeFileSync(path.join(settingspath, 'notes.json'), JSON.stringify(loadNotes));
        //win.webContents.openDevTools();
    }
    sHSettings() {
        if (!this.showControls){
            document.getElementById('hvState').style.display = 'block';
            document.getElementById('txtarea').style.height = 'calc(100vh - 75px)';
            this.showControls = true;
        } else {
            document.getElementById('hvState').style.display = 'none';
            document.getElementById('txtarea').style.height = 'calc(100vh - 45px)';
            this.showControls = false;
        }
    }
}
